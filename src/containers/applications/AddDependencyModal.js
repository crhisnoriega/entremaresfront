import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CustomInput,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Label
} from "reactstrap";
import Select from "react-select";
import CustomSelectInput from "../../components/common/CustomSelectInput";
import IntlMessages from "../../helpers/IntlMessages";

class AddDependencyModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      label: {},
      labelColor: "",
      category: {},
      status: "PENDING"
    };
  }

  addNetItem = () => {
    const newItem = {
      title: this.state.title,
      label: this.state.label.value,
      labelColor: this.state.label.color,
      category: this.state.category.value,
      status: this.state.status
    };

    this.props.callback(newItem);
    this.props.toggleModal();
    
    this.setState({
      title: "",
      label: {},
      category: {},
      status: "ACTIVE"
    });
  };

  render() {
    console.log("render");
    console.log(this.props.surveyListApp);
    const { labels, categories } = this.props.surveyListApp;
    const { modalOpen, toggleModal } = this.props;
    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
        wrapClassName="modal-right"
        backdrop="static"
      >
        <ModalHeader toggle={toggleModal}>Registrar Dependente</ModalHeader>
        <ModalBody>
          <Label className="mt-4">Nome</Label>
          <Input
            type="text"
            defaultValue={this.state.title}
            onChange={event => {
              this.setState({ title: event.target.value });
            }}
          />

          <Label className="mt-4">Tipo Depedencias</Label>
          <Select
            components={{ Input: CustomSelectInput }}
            className="react-select"
            classNamePrefix="react-select"
            name="form-field-name"
            options={categories.map((x, i) => {
              return { label: x, value: x, key: i };
            })}
            value={this.state.category}
            onChange={val => {
              this.setState({ category: val });
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" outline onClick={toggleModal}>
            Cancelar
          </Button>
          <Button color="primary" onClick={() => this.addNetItem()}>
            Confirmar
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default AddDependencyModal;
