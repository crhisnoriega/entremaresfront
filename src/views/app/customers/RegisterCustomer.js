import React, { Component } from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {
  FormikReactSelect,
  FormikCheckboxGroup,
  FormikCheckbox,
  FormikRadioButtonGroup,
  FormikCustomCheckbox,
  FormikCustomCheckboxGroup,
  FormikCustomRadioGroup,
  FormikTagsInput,
  FormikSwitch,
  FormikDatePicker
}
from "../FormikFields";
import {
  Card,
  CardBody,
  Row,
  FormGroup,
  Label,
  Button,
  Input,
  FormFeedback,
  CustomInput,
  Table
}
from "reactstrap";
import AddDependencyModal from "../../../containers/applications/AddDependencyModal";

import { Colxx, Separator } from "../../../components/common/CustomBootstrap";
import ReactSiemaCarousel from "../../../components/ReactSiema/ReactSiemaCarousel";

import IntlMessages from "../../../helpers/IntlMessages";

import Breadcrumb from "../../../containers/navs/Breadcrumb";

import InputMask from "react-input-mask";
import request from "sync-request";
import axios from "axios";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

const _init = {
  codigo: "",
  name: "",
  cpf: "",
  rg: "",
  nacionalidade: "",

  shortName: "",
  description: "",

  cep: "",
  street: "",
  number: "",
  complemento: "",
  bairro: "",
  city: "",
  state: "",
  email: "",
  fone1: "",
  fone2: "",

  civil: null,
  plano: null
};
const SignupSchema = Yup.object().shape({
  name: Yup.string().required("Nome requerido"),
  cpf: Yup.string().required("CPF requerido"),
  rg: Yup.string().required("RG requerido"),
  nacionalidade: Yup.string().required("Nacionalidade requerida"),

  civil: Yup.object()
    .typeError("Estado civil requerido")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),

  plano: Yup.object()
    .typeError("Plano requerido")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),

  fone1: Yup.string().required("Telefone requerido"),
  email: Yup.string().required("Email requerido"),

  cep: Yup.string().required("CEP requerido"),
  street: Yup.string().required("Rua requerida"),
  number: Yup.string().required("Numero requerido"),
  city: Yup.string().required("Cidade requerido")
});

const plan_list = [
  { value: "bronze", label: "Bronze" },
  { value: "prata", label: "Prata" },
  { value: "ouro", label: "Ouro" }
];

const categories_list = [
  { value: "apartamento", label: "Apartamento " },
  { value: "casa", label: "Casa" },
  { value: "kitnet", label: "Kitnet" },
  { value: "loft", label: "Loft" }
];

class FormikCustomComponents extends Component {
  constructor(props) {
    super(props);

    this.customer_id = null;

    this.state = {
      init: _init,
      images: [],
      modalOpen: false,
      dependentes: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.customer_id = localStorage.getItem("customer_id");

    console.log(this.customer_id);

    if (this.customer_id) {
      axios
        .get("/customer/" + this.customer_id, {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          console.log(res.data);
          res.data.nacionalidade = res.data.nacio ? res.data.nacio : "brasileiro";
          this.setState({ init: res.data, codigo: res.data.codigo, dependentes: res.data.dependency });
        })
        .catch(error => {
          console.log(error);
        });
    }
    else {
      axios
        .get("/customer/counter", {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          this.setState({ codigo: "00000" + res.data.counter });
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  handleSubmit = (values, { setSubmitting }) => {
    values.codigo = this.state.codigo;
    values.dependency = this.state.dependentes;
    values.nacio = values.nacionalidade;

    console.log(values);

    if (this.customer_id) {
      axios
        .put("/customer/" + this.customer_id, values, {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          this.setState({ init: _init });
          this.props.history.push("/app/customer/list");
          localStorage.removeItem("customer_id")
          setSubmitting(false);
        })
        .catch(error => {
          console.log(error);
          setSubmitting(false);
        });
    }
    else {
      axios
        .post("/customer", values, {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          this.setState({ init: _init });
          this.props.history.push("/app/customer/list");
          setSubmitting(false);
        })
        .catch(error => {
          console.log(error);
          setSubmitting(false);
        });
    }
  };

  removeImage = image => {
    let filteredArray = this.state.images.filter(
      item => item.name !== image.name
    );
    this.setState({ images: filteredArray, filename: "" });
  };

  handleFileSelect = e => {
    console.log(e.target.files[0].name);
    let file = e.target.files[0];

    // Make new FileReader
    let reader = new FileReader();

    // Convert the file to base64 text
    reader.readAsDataURL(file);

    // on reader load somthing...
    reader.onload = () => {
      // Make a fileInfo Object
      let fileInfo = {
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + " kB",
        base64: reader.result,
        file: file
      };

      this.setState({
        images: [fileInfo, ...this.state.images]
      });
    };
  };

  onImageUpload = (fileinfo, productid) => {
    alert(fileinfo);

    axios
      .post("/properties/base64image/" + productid, fileinfo, {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(function() {
        console.log("SUCCESS!!");
      })
      .catch(error => {
        console.log(error);
      });
  };

  toggleModal = () => {
    this.setState({
      modalOpen: !this.state.modalOpen
    });
  };

  handleAddItem = e => {
    this.setState({ dependentes: [e, ...this.state.dependentes] });
    console.log(e);
  };

  render() {
    let list_dependentes = this.state.dependentes.map((dep, i) => {
      return (
        <tr>
          <th scope="row">{i + 1}</th>
          <td>{dep.title}</td>
          <td>{dep.category}</td>
          <td>
            {
              <Button
                onClick={e => {
                  var newArray = this.state.dependentes.filter(
                    dep1 => dep1.title != dep.title
                  );
                  this.setState({ dependentes: newArray });
                }}
                color="danger"
              >
                Remover
              </Button>
            }
          </td>
        </tr>
      );
    });

    console.log("state: " + this.state.images);
    console.log("init: " + JSON.stringify(this.state.init));
    return (
      <Row className="mb-4">
        <AddDependencyModal
          callback={this.handleAddItem}
          toggleModal={this.toggleModal}
          modalOpen={this.state.modalOpen}
          surveyListApp={{
            labels: [{ value: "filho", label: "Filho" }],
            categories: ["Filho", "Mae", "Avo", "Avó"]
          }}
        />
        <Row>
          <Colxx xxs="12">
            <Breadcrumb
              heading="Cadastro de Cliente"
              match={this.props.match}
            />
            <Separator className="mb-5" />
          </Colxx>
        </Row>
        <Colxx xxs="12">
          <Card>
            <CardBody>
              <Formik
                enableReinitialize
                initialValues={this.state.init}
                validationSchema={SignupSchema}
                onSubmit={this.handleSubmit}
              >
                {({
                  handleSubmit,
                  setFieldValue,
                  setFieldTouched,
                  handleChange,
                  handleBlur,
                  values,
                  errors,
                  touched,
                  isSubmitting
                }) => (
                  <Form className="av-tooltip tooltip-label-right">
                    <FormGroup>
                      <Label className="green-label" for="codigo">
                        Codigo do Cliente
                      </Label>
                      <Input
                        disabled
                        className="col-md-4"
                        invalid={touched.codigo && !!errors.codigo}
                        type="text"
                        name="codigo"
                        id="codigo"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={this.state.codigo}
                      />
                      <FormFeedback>{errors.codigo}</FormFeedback>
                    </FormGroup>

                    <Row>
                      <Colxx xs="4" md="3">
                        <FormGroup className="error-l-100">
                          <Label>Plano do Cliente</Label>
                          <FormikReactSelect
                            name="plano"
                            id="plano"
                            value={values.plano}
                            isMulti={false}
                            options={plan_list}
                            onChange={setFieldValue}
                            onBlur={setFieldTouched}
                          />
                          {errors.plano && touched.plano ? (
                            <div className="invalid-feedback d-block">
                              {errors.plano}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Colxx>
                    </Row>

                    <FormGroup className="error-l-140">
                      <Label className="green-label error-l-300" for="name">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                      {errors.name && touched.name ? (
                        <div className="invalid-feedback d-block">
                          {errors.name}
                        </div>
                      ) : null}
                    </FormGroup>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="cpf">
                          CPF
                        </Label>
                        <Input
                          invalid={touched.cpf && !!errors.cpf}
                          type="text"
                          name="cpf"
                          id="cpf"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.cpf}
                          mask="999.999.999-99"
                          maskChar=" "
                          tag={InputMask}
                        />
                        {errors.cpf && touched.cpf ? (
                          <div className="invalid-feedback d-block">
                            {errors.cpf}
                          </div>
                        ) : null}
                      </FormGroup>
                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="rg">
                          RG
                        </Label>
                        <Input
                          invalid={touched.rg && !!errors.rg}
                          type="text"
                          name="rg"
                          id="rg"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.rg}
                          mask="99.999.999-9"
                          maskChar=" "
                          tag={InputMask}
                        />
                        {errors.rg && touched.rg ? (
                          <div className="invalid-feedback d-block">
                            {errors.rg}
                          </div>
                        ) : null}
                      </FormGroup>
                    </Row>
                    <Row
                      style={{ marginBottom: "20px" }}
                      form
                      className="col-md-15"
                    >
                      <Colxx xxs="6">
                        <FormGroup>
                          <Label className="green-label" for="email">
                            Dependentes
                          </Label>

                          <Table striped>
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>Tipo</th>
                                <th />
                              </tr>
                            </thead>
                            <tbody>{list_dependentes}</tbody>
                          </Table>
                        </FormGroup>

                        <Button
                          color="primary"
                          onClick={e => {
                            this.setState({ modalOpen: true });
                          }}
                        >
                          Adicionar Dependente
                        </Button>
                      </Colxx>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="nacionalidade">
                          Nacionalidade
                        </Label>
                        <Input
                          invalid={
                            touched.nacionalidade && !!errors.nacionalidade
                          }
                          type="text"
                          name="nacionalidade"
                          id="nacionalidade"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.nacionalidade}
                        />
                        {errors.nacionalidade && touched.nacionalidade ? (
                          <div className="invalid-feedback d-block">
                            {JSON.stringify(errors.nacionalidade)}
                          </div>
                        ) : null}
                      </FormGroup>

                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="civil">
                          Estado Civil
                        </Label>
                        <FormikReactSelect
                          name="civil"
                          id="civil"
                          value={values.civil}
                          isMulti={false}
                          options={[
                            { value: "solteiro", label: "Solteiro" },
                            { value: "casado", label: "Casado" }
                          ]}
                          onChange={setFieldValue}
                          onBlur={setFieldTouched}
                        />
                        {errors.civil && touched.civil ? (
                          <div className="invalid-feedback d-block">
                            {errors.civil}
                          </div>
                        ) : null}
                      </FormGroup>
                    </Row>

                    <FormGroup>
                      <Label className="green-label" for="description">
                        Observação
                      </Label>
                      <Input
                        className="col-md-10"
                        name="description"
                        type="textarea"
                        invalid={touched.description && !!errors.description}
                        type="text"
                        id="description"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.description}
                      />
                      {errors.description && touched.description ? (
                        <div className="invalid-feedback d-block">
                          {errors.description}
                        </div>
                      ) : null}
                    </FormGroup>

                    <Row form className="col-md-15">
                      <FormGroup className="col-3">
                        <Label className="green-label" for="cep">
                          CEP
                        </Label>
                        <Input
                          invalid={touched.cep && !!errors.cep}
                          type="text"
                          name="cep"
                          id="cep"
                          onChange={e => {
                            handleChange(e);

                            var cep = e.target.value;
                            cep = cep.replace("-", "");
                            cep = cep.replace(" ", "");

                            if (cep.length == 8) {
                              var res = request(
                                "GET",
                                "https://viacep.com.br/ws/" + cep + "/json/"
                              );

                              console.log(res);
                              var add = JSON.parse(res.body);
                              values.street = add.logradouro;
                              values.bairro = add.bairro;
                              values.city = add.localidade;
                            }
                          }}
                          onBlur={handleBlur}
                          value={values.cep}
                          mask="99999-999"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.cep}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-6">
                        <Label className="green-label" for="street">
                          Logradouro
                        </Label>
                        <Input
                          invalid={touched.street && !!errors.street}
                          type="text"
                          name="street"
                          id="street"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.street}
                        />
                        <FormFeedback>{errors.street}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-2">
                        <Label className="green-label" for="number">
                          Numero
                        </Label>
                        <Input
                          invalid={touched.number && !!errors.number}
                          type="text"
                          name="number"
                          id="number"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.number}
                        />
                        <FormFeedback>{errors.number}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-3">
                        <Label className="green-label" for="complemento">
                          Complemento
                        </Label>
                        <Input
                          invalid={touched.complemento && !!errors.complemento}
                          type="text"
                          name="complemento"
                          id="complemento"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.complemento}
                        />
                        <FormFeedback>{errors.complemento}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-5">
                        <Label className="green-label" for="bairro">
                          Bairro
                        </Label>
                        <Input
                          invalid={touched.bairro && !!errors.bairro}
                          type="text"
                          name="bairro"
                          id="bairro"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.bairro}
                        />
                        <FormFeedback>{errors.bairro}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-5">
                        <Label className="green-label" for="city">
                          Cidade
                        </Label>
                        <Input
                          invalid={touched.city && !!errors.city}
                          type="text"
                          name="city"
                          id="city"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.city}
                        />
                        <FormFeedback>{errors.city}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="error-l-100">
                        <Label>Estado </Label>
                        <select
                          name="select"
                          className="form-control"
                          value={values.select}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        >
                          <option value="">Seleção</option>
                          <option value="1">SP</option>
                          <option value="2">RJ</option>
                          <option value="3">RS</option>
                          <option value="4">MG</option>
                          <option value="5">BA</option>
                        </select>

                        {errors.select && touched.select ? (
                          <div className="invalid-feedback d-block">
                            {errors.select}
                          </div>
                        ) : null}
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-5">
                        <Label className="green-label" for="fone1">
                          Telefone 1
                        </Label>
                        <Input
                          invalid={touched.fone1 && !!errors.fone1}
                          type="text"
                          name="fone1"
                          id="fone1"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.fone1}
                          mask="(99) 99999-9999"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.fone1}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-5">
                        <Label className="green-label" for="fone2">
                          Telefone 2
                        </Label>
                        <Input
                          invalid={touched.fone2 && !!errors.fone2}
                          type="text"
                          name="fone2"
                          id="fone2"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.fone2}
                          mask="(99) 99999-9999"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.fone2}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-8">
                        <Label className="green-label" for="email">
                          Email
                        </Label>
                        <Input
                          invalid={touched.email && !!errors.email}
                          type="text"
                          name="email"
                          id="email"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.email}
                        />
                        <FormFeedback>{errors.email}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <LaddaButton
                      type="submit"
                      className="btn btn-primary btn-ladda"
                      loading={isSubmitting}
                      onClick={e => {
                        handleSubmit(e);
                      }}
                    >
                      Confirmar
                    </LaddaButton>
                  </Form>
                )}
              </Formik>
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    );
  }
}
export default FormikCustomComponents;
