import React, { Component } from "react";
import { Row, Card, CardBody, CardHeader } from "reactstrap";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import axios from "axios";
import "react-big-calendar/lib/css/react-big-calendar.css";

import { Colxx, Separator } from "../../../components/common/CustomBootstrap";
import Breadcrumb from "../../../containers/navs/Breadcrumb";
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));

const currDate = new Date();
const currYear = currDate.getFullYear();
const currMonth = currDate.getMonth();

const events = [{
  title: "Reserva Casa Praia - Ubatuba",
  allDay: true,
  start: new Date(currYear, currMonth, 0),
  end: new Date(currYear, currMonth, 4)
}];

// todo: reactive custom calendar toolbar component

class Calendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      events: []
    };
  }

  componentDidMount() {
    axios
      .get("/reservation/search/complete?query=%", {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var as_events = [];

        res.data.forEach(element => {
          as_events.push({
            title: element.customer.name + " - " + element.property.name,
            allDay: true,
            start: Date.parse(element.reservation.start_date),
            end: Date.parse(element.reservation.end_date)
          });
        });

        this.setState({ events: as_events });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="animated">
        <Row>
          <Colxx xxs="12">
            <Breadcrumb
              heading="Calendario de Reservas"
              match={this.props.match}
            />
            <Separator className="mb-5" />
          </Colxx>
        </Row>
        <Card>
          <CardHeader />
          <CardBody style={{ height: "40em" }}>
            <BigCalendar
              className="d-sm-down-none"
              {...this.props}
              events={this.state.events}
              views={["month", "week", "day"]}
              step={30}
              defaultDate={new Date(currYear, currMonth, 1)}
              defaultView="month"
              toolbar={true}
              messages={{
                month: "Mes",
                day: "Dia",
                today: "Hoje",
                week: "Semana",
                previous: "Anterior",
                next: "Próximo"
              }}
              onSelectEvent={e => console.log(e)}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Calendar;
