import React from "react";
import { Card, CustomInput, Badge, Button, } from "reactstrap";
import { NavLink } from "react-router-dom";
import classnames from "classnames";
import { ContextMenuTrigger } from "react-contextmenu";
import { Colxx } from "../../../components/common/CustomBootstrap";

import moment from "moment";

const DataListViewCustomer = ({ reservation, customer, property, isSelect, collect, onCheckItem, onChangeStatus }) => {
  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={reservation.id} collect={collect}>
        <Card
          onClick={event => onCheckItem(event, reservation.id)}
          className={classnames("d-flex flex-row", {
            active: isSelect
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <NavLink to={`/app/reservation/register`} className="">
                <p className="list-item-heading mb-2" style={{width:"200px"}}>{property.name}</p>
              </NavLink>
              
              <NavLink to={`/app/reservation/register`} className="">
                <p className="list-item-heading text-muted  mb-1 truncate">{customer.name}</p>
              </NavLink>
              
              
              
                <p className="list-item-heading text-muted  text-small mb-1 truncate">{reservation.status}</p>
              

              
 
              <p className="mb-1 text-muted text-small w-20 w-sm-100">
                {moment(reservation.start_date).format("DD/MM/YYYY")}{" - "}{moment(reservation.end_date).format("DD/MM/YYYY")}
              </p>
              
              <p>
                  <Button
                          color="danger"
                          onClick={e => {
                            e.stopPropagation();
                            onChangeStatus(reservation);
                          }}
                        >
                          Alterar Status
                        </Button>
              </p>


             
            
            </div>
            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
              <CustomInput
                className="item-check mb-0"
                type="checkbox"
                id={`check_${reservation.id}`}
                checked={isSelect}
                onChange={() => {}}
                label=""
              />
            </div>
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(DataListViewCustomer);
